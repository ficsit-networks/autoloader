function listDrives(drives)
    for i = 1, #drives do
        print(drives[i])
    end
end

function downloadData(card, url)
	local req = card:request(url, "GET", "")
	local _, libdata = req:await()
	return libdata
end

-- Initialize /dev
if filesystem.initFileSystem('/dev') == false then
    computer.panic('Cannot initialize /dev')
end

local drives = filesystem.childs('/dev')

if #drives == 0 then
    computer.panic('No drives found')
end

listDrives(drives)

filesystem.mount('/dev/'..drives[1], '/')



local fileName = '/init.lua'
if not filesystem.exists(fileName) then
    computer.panic('File not found: ' .. fileName)
end

--filesystem.doFile(fileName)

local card = computer.getPCIDevices(classes["FINInternetCard"])[1]
-- print(card)

-- https://raw.githubusercontent.com/rxi/json.lua/master/bench/bench_all.lua
-- get library from internet
-- local req = card:request("https://raw.githubusercontent.com/rxi/json.lua/master/bench/bench_all.lua", "GET", "")
-- local _, libdata = req:await()

local data = downloadData(card, "https://raw.githubusercontent.com/rxi/json.lua/master/bench/bench_all.lua")
print(data)

